# Tutorial para la publicación desde una sola fuente

Esta publicación es un tutorial para producir un libro en
formato HTML, EPUB y PDF
a partir de un archivo [Markdown][] según la metodología de
[publicación desde una sola fuente][] y con herramientas que
son [_software_ libre][] o de [código abierto][]. El flujo
de trabajo es el siguiente:

```mermaid
graph TD;
    A[Proyecto editorial] -->|Edición con Zettlr| Markdown;
    Markdown -->|Conversión con Pandoc| EPUB & HTML & SLA;
    SLA -->|Maquetación con Scribus| PDF;
```

Este tutorial fue redactado para la producción de libros
multiformato de [Adjetiva Editorial][]. Sin embargo, también
es relevante para las personas interesadas en esta
metodología de producción o para quienes desean aprender a
hacer libros con [_software_ libre][] pero que no cuentan con
un sistema operativo [GNU/Linux][GNU/Linux] o no quieren
recurrir al uso de una [terminal][].
Si cuentas con experiencia, este tutorial también puede
hacerse con tu editor favorito y tu terminal de preferencia
sin necesidad de modificar el árbol de directorios o los
archivos de configuración.

Este tutorial es un muestra de las capacidades de este tipo
de publicación, por lo que está para consulta y evaluación
en tres formatos:

* [En línea][Tutorial en línea]
* [En EPUB][Tutorial en EPUB]
* [En PDF][Tutorial en PDF]

Para poder realizar este tutorial necesitas:

1. Una computadora sin importar su sistema operativo.
2. Conexión a internet para la descarga del _dummy_ y
   del _software_ necesario.

## Licencia

Este tutorial está bajo [Licencia Editorial Abierta y Libre (LEAL)][LEAL]. Con LEAL eres libre de usar, copiar, reeditar, modificar, distribuir o comercializar bajo las siguientes condiciones:

* Los productos derivados o modificados han de heredar algún tipo de LEAL.
* Los archivos editables y finales habrán de ser de acceso público.
* El contenido no puede implicar difamación, explotación o vigilancia.

[Markdown]: https://es.wikipedia.org/wiki/Markdown
[publicación desde una sola fuente]: https://es.wikipedia.org/wiki/Publicaci%C3%B3n_desde_una_sola_fuente
[_software_ libre]: https://es.wikipedia.org/wiki/Software_libre
[código abierto]: https://es.wikipedia.org/wiki/C%C3%B3digo_abierto
[Adjetiva Editorial]: https://adjetiva.mx
[GNU/Linux]: https://es.wikipedia.org/wiki/GNU/Linux
[terminal]: https://es.wikipedia.org/wiki/Interfaz_de_l%C3%ADnea_de_comandos
[Tutorial en línea]: https://prolibreros.gitlab.io/docs/tutorial-ssp
[Tutorial en EPUB]: https://prolibreros.gitlab.io/docs/tutorial-ssp/tutorial-ssp.epub
[Tutorial en PDF]: https://prolibreros.gitlab.io/docs/tutorial-ssp/tutorial-ssp.pdf
[LEAL]: https://programando.li/bres
