# Instalación de herramientas libres

Para este tutorial vamos a requerir la instalación de dos
herramientas libres:

1.  [Zettlr]
2.  [Scribus]

::: {.note .centrado}
Para saber qué es una «herramienta libre», lee el [apéndice
2].
:::

[Zettlr] es un editor de [Markdown] multiplataforma que
incluye la navaja suiza para la conversión de documentos:
[Pandoc]. Con [Zettlr] trabajaremos el tutorial con
[Markdown] para después convertirlo a [HTML]{.acronimo},
[EPUB]{.acronimo} y [SLA]{.acronimo} con [Pandoc].

::: {.note .centrado}
[Descarga Zettlr aquí]
:::

[Scribus] es un programa de [*desktop publishing*] que nos
permitirá maquetar el tutorial para tener una salida
[PDF]{.acronimo} para impresión. Con [Pandoc] y [Zettlr] de
manera automatizada podríamos tener una salida
[PDF]{.acronimo} mediante [TeX], un sistema de composición
de textos célebre entre las comunidades de [*software*
libre] o de [código abierto]. Sin embargo, para este
tutorial optamos por el camino manual que nos ofrece
[Scribus].

::: {.note .centrado}
[Descarga Scribus aquí]
:::

El objetivo de este tutorial es el uso de la metodología
[SSP]{.acronimo} con la experiencia de usuario más amena
disponible. [TeX] nos ofrece más posibilidades que
[Scribus], además de estar respaldado por una amplia
comunidad que nos facilita encontrar soporte. No obstante,
la metodología para la composición de documentos con [TeX]
dista mucho de la manera habitual en la que se forman
documentos para impresión según las pautas del diseño
gráfico. Por otro lado, [TeX] cuenta con una curva de
aprendizaje escarpada que no lo hace apto para un primer
encuentro con la metodología [SSP]{.acronimo}.

::: note
La metodología [SSP]{.acronimo} es un flujo de trabajo que
nos permite la [publicación desde una sola fuente]. En
inglés se conoce como *single-source publishing* o
[SSP]{.acronimo}. Con esta metodología puedes utilizar el
mismo documento para exportarlo a diversos formatos sin
comprometer la calidad técnica o el cuidado editorial. Si te
interesa saber más, lee el [apartado anterior].
:::

Por estos motivos hemos escogido [Scribus]. Pese a sus
limitaciones, [Scribus] nos ofrece una interfaz gráfica que
rápidamente nos permitirá obtener un ritmo de trabajo afín a
los objetivos de este tutorial.

Cabe resaltar que el uso de [Scribus] no compite con el
empleo de [TeX]. Con [Zettlr] te darás cuenta que puedes
compilar la fuente a [PDF]{.acronimo}/[TeX] e incluso a
[IDML]{.acronimo} para su maquetación con Adobe InDesign.
Aunque en este tutorial no cubrimos cómo trabajar la
exportación a [PDF]{.acronimo}/[TeX] o [IDML]{.acronimo}, sí
tendrás los elementos necesarios para experimentar y ajustar
la exportación por tu propia cuenta.

## Instalación de Zettlr

La instalación de [Zettlr] es muy sencilla, solo tienes que
ir a su [página de descarga][Descarga Zettlr aquí]. Aunque
si tienes alguna duda, puedes consultar las siguientes
secciones.

### Instalación en Windows

Para instalar [Zettlr] en Windows realiza los siguientes
pasos:

1.  Descarga el archivo [EXE]{.acronimo} `WINDOWS (X86_64)`.
2.  Instala el archivo.

::: warn
Si por algún motivo no pudiste realizar la instalación,
prueba con el archivo `WINDOWS (ARM64)`.
:::

### Instalación en MacOS

Para instalar [Zettlr] en MacOS realiza los siguientes
pasos:

1.  Descarga el archivo [DMG]{.acronimo} `INTEL (X86_64)`.
2.  Instala el archivo.

::: warn
Si por algún motivo no pudiste realizar la instalación,
prueba con el archivo `APPLE SILICON (ARM64)`.
:::

::: note
Si cuentas con el gestor de paquetes [Homebrew], prefiere
este método de instalación con:

    brew install --cask Zettlr
:::

### Instalación en [GNU]{.acronimo}/Linux

Debido a que la instalación de [Zettlr] en
[[GNU]{.acronimo}/Linux][1] depende del tipo de
distribución, aquí solo explicamos el método que funciona
para cualquier distribución:

1.  Descarga el archivo AppImage `APPIMAGE (X86_64)`.
2.  Ve al directorio donde está el archivo desde tu
    [terminal].
3.  Da permiso de ejecución al archivo con
    `chmod a+x Zettlr*.AppImage`.
4.  Ejecuta con `./Zettlr*.AppImage`.

::: note
[Zettlr] cuenta con paquetes para [Debian] ---que incluye a
[Linux Mint] o [Ubuntu]---, [Fedora] o [Arch Linux]. Si
cuentas con alguna de estas distribuciones, prefiere este
método de instalación. Consulta más al respecto en su
[página de descarga][Descarga Zettlr aquí].
:::

## Instalación de Scribus

La instalación de [Scribus] es muy sencilla, solo tienes que
ir a su [página de descarga][Descarga Scribus aquí]. Aunque
si tienes alguna duda, puedes consultar las siguientes
secciones.

### Instalación en Windows

Para instalar [Scribus] en Windows realiza los siguientes
pasos:

1.  Ve a [su repositorio].
2.  Descarga el archivo [EXE]{.acronimo} **que termina** con
    `windows-x64.exe`.
3.  Instala el archivo.

::: warn
Si por algún motivo no pudiste realizar la instalación,
prueba con el archivo **que termina** con `windows.exe`.
:::

### Instalación en MacOS

Para instalar [Scribus] en MacOS realiza los siguientes
pasos:

1.  Ve a [su repositorio].
2.  Descarga el archivo [DMG]{.acronimo} **que termina** con
    `.dmg`.
3.  Instala el archivo.

### Instalación en [GNU]{.acronimo}/Linux

Aunque en [su repositorio] esta disponible el código para su
compilación. Las maneras más sencillas para instalar
[Scribus] son las siguientes.

Si cuentas con [Debian], [Linux Mint] o [Ubuntu], en tu
terminal ejecuta:

    sudo apt install scribus

Si cuentas con [Arch Linux], en tu terminal ejecuta:

    sudo pacman install scribus

::: warn
En algunas versiones de [Scribus] existe un conflicto con su
pantalla de bienvenida ([*splash screen*]) que puede impedir
su arranque. Para solucionar este problema en tu terminal
ejecuta:

    scribus --never-splash

Con este comando la pantalla de bienvenida quedará
deshabilitada. A partir de ahora puedes abrir [Scribus] sin
necesitar la terminal.
:::

  [Zettlr]: https://www.zettlr.com
  [Scribus]: https://www.scribus.net
  [apéndice 2]: #apéndice-2.-herramientas-libres-para-la-edición
  [Markdown]: https://es.wikipedia.org/wiki/Markdown
  [Pandoc]: https://pandoc.org
  [Descarga Zettlr aquí]: https://www.zettlr.com/download
  [*desktop publishing*]: https://en.wikipedia.org/wiki/Desktop_publishing
  [TeX]: https://es.wikipedia.org/wiki/TeX
  [*software* libre]: https://es.wikipedia.org/wiki/Software_libre
  [código abierto]: https://es.wikipedia.org/wiki/C%C3%B3digo_abierto
  [Descarga Scribus aquí]: https://www.scribus.net/downloads/stable-branch
  [publicación desde una sola fuente]: https://es.wikipedia.org/wiki/Publicaci%C3%B3n_desde_una_sola_fuente
  [apartado anterior]: #metodología-ssp
  [Homebrew]: https://brew.sh
  [1]: https://es.wikipedia.org/wiki/GNU/Linux
  [terminal]: https://es.wikipedia.org/wiki/Interfaz_de_l%C3%ADnea_de_comandos
  [Debian]: https://www.debian.org
  [Linux Mint]: https://linuxmint.com
  [Ubuntu]: https://ubuntu.com
  [Fedora]: https://getfedora.org
  [Arch Linux]: https://www.archlinux.org
  [su repositorio]: https://sourceforge.net/projects/scribus/files/scribus/1.4.8
  [*splash screen*]: https://en.wikipedia.org/wiki/Splash_screen
