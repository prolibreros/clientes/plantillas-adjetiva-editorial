# Metodología [SSP]{.acronimo}

Según [la entrada] de la Wikipedia en español:

> La **publicación desde una sola fuente** (del inglés
> *single-source publishing*) o **[SSP]{.acronimo}** (su
> acrónimo en inglés) es un método de administración de
> contenido que permite la publicación ilimitada de un
> documento en diferentes soportes de medios.

Con la [SSP]{.acronimo} es posible hacer un libro en
diversos formatos ---como [HTML]{.acronimo} para *web*,
[EPUB]{.acronimo} para *ereaders* y [PDF]{.acronimo} para
impresión--- utilizando los mismos archivos y a través de
una producción que puede ser automatizada de manera parcial
o total. Esta metodología está pensada para hacer más
eficiente la administración de contenidos y de los recursos
disponibles, ya que pretende publicar un conjunto de
formatos que en una metodología convencional de publicación
puede llevar a un mayor uso de recursos técnicos o a una
pérdida de calidad editorial.

Entonces, quizá una manera de entender qué es la
[SSP]{.acronimo} sea a partir de las dificultades que
existen en la publicación contemporánea de libros.

::: espacio-arriba
Durante varios siglos los libros solo requerían un soporte
para su distribución o comercialización: el papel impreso.
Esto produjo una interesante historia de perfeccionamiento
técnico y de conocimiento editorial en los que la producción
de un libro puede percibirse de manera somera en estos
pasos:
:::

1.  Redacción
2.  Edición
3.  Diseño
4.  Revisión
5.  Impresión
6.  Distribución

Antes de la implementación de recursos computacionales en la
edición, la reversibilidad en estos procesos tendía a ser
costosa o a requerir de cierto ingenio para enmendar
errores. Por ejemplo, si en el momento de la impresión se
encontraban erratas... alguien podría perder su trabajo o
terminar en bancarrota, así como es probable que el editor
incorporaría una [fe de erratas].

![Titivillus, el demonio de la edición, célebre por ser el
culpable de las erratas en los libros desde la Edad Media.
En la actualidad es el responsable de la producción de
libros de la entidad editorial que más títulos en español
publica al año: [epublibre], una comunidad de amantes de la
lectura que en \~10 años ha publicado más de 55 mil
libros.][1]

::: espacio-arriba
En las últimas décadas del siglo [XX]{.acronimo} los
procesos editoriales tradicionales empezaron a incorporar
los recursos de cómputo para la publicación de libros. Esta
migración no se hizo sin resistencia, varias personas en la
edición padecieron ---y algunas aún padecen--- del «síndrome
de Trithemius», una actitud de «rechazo a lo novedoso en
relación con la cultura escrita».[^1]
:::

Una de las principales ventajas en esta adopción tecnológica
es la capacidad de concentrar varios procesos en una máquina
---la computadora--- que permite mayor reversibilidad. Por
ejemplo, con el uso del [*desktop publishing*] es posible
concentrar los procesos de redacción, edición, diseño y
revisión en un solo programa que, con el cuidado adecuado,
permite revertir procesos.

Sin embargo, esta concentración de los procesos implicó un
[*trade-off*]. Mientras el uso de *software* en la edición
empezó a establecerse como el modelo de producción por
defecto, surgió la necesidad de ofrecerle al público otro
soporte distinto al impreso: el «libro electrónico».

Con esta necesidad, las personas dedicadas a la producción
de publicaciones al menos han tenido que redoblar esfuerzos
para la producción de un formato adicional. Para ello, por
lo general se lleva a cabo un primer ciclo de producción en
el cual se obtiene un [PDF]{.acronimo} para impresión.
Después se inicia un nuevo bucle para la producción de un
soporte electrónico, como puede ser un archivo en formato
[EPUB]{.acronimo}, [XML]{.acronimo} o [HTML]{.acronimo}.

En esta metodología cíclica, cada bucle recicla el documento
de un formato anterior. Por ejemplo, varias editoriales
reusan el archivo [INDD]{.acronimo} con el que produjeron el
[PDF]{.acronimo} de impresión para convertirlo en
[EPUB]{.acronimo}.

Esta metodología de publicación ayuda a salvar costos de
producción, pero poco o nada contribuye para preservar la
calidad técnica y editorial de cada nuevo formato producido.
Además, es común que el control técnico disminuya y la
calidad editorial sea inferior en cada nuevo bucle porque
cada formato tiene especificaciones distintas que en su
reuso heredan elementos que pueden producir formatos
inválidos o incluso ser perceptibles al lector como erratas.

Por ejemplo, en el [INDD]{.acronimo} para un
[PDF]{.acronimo} de impresión de manera rutinaria se añaden
saltos de línea forzados para mejorar la estética de algún
párrafo. Sin embargo, para un [EPUB]{.acronimo} hecho a
partir del [INDD]{.acronimo} estos saltos pueden producir
vistas indeseadas ya que su «página» no es un espacio fijo,
sino que varía según el tamaño del dispositivo.

Aunque es posible realizar procesos para el control de
calidad en cada bucle ---como la eliminación de saltos de
línea forzados---, por lo general se carece de certeza sobre
la calidad final del formato. Por ello es que en
[Programando [LIBRE]{.acronimo}ros][2] hemos desistido en
este modelo de producción multiformato.

![Ejemplo de publicación cíclica, en cada nuevo bucle se
aumenta la probabilidad de un menor control técnico y una
inferior calidad editorial.]

::: espacio-arriba
En los noventa la documentación de *software* empezó a tener
necesidades multiformato y de publicación continua. Un
programa de cómputo tiende a contar con ciclos de desarrollo
en donde cada semana o mes se publican nuevas versiones que
lo actualizan o le arreglan [*bugs*]. Una nueva versión de
*software* también exige la actualización de su
documentación si el interés es mantener al usuario al tanto
de estas modificaciones.
:::

El requisito de una periodicidad semanal o mensual para la
publicación multiformato llevó a la industria del *software*
a elaborar metodologías de publicación. Estos flujos de
trabajo partieron de sus conocimientos en el manejo de
recursos para la compilación de un programa en varios
destinos (*targets*).

Por ejemplo, los programas que utilizaremos en este tutorial
([Zettlr], [Pandoc] y [Scribus]) son multiplataforma porque
están disponibles para Windows, MacOS y
[[GNU]{.acronimo}/Linux][3]. Sin embargo, esto no quiere
decir que tengan una base de código distinta para cada uno
de los sistemas operativos donde puede ser utilizado. Al
contrario, cada *software* tiene una sola fuente de código
que se compila para diferentes plataformas cada vez que se
publica una nueva versión.

De manera análoga a partir de una sola fuente se «compila»
el texto en distintos formatos para su impresión, su lectura
en un *ereader* o su disponibilidad en un sitio *web*. En
este flujo de trabajo para la publicación multiformato no
hay bucles que reutilicen el contenido de un formato previo
para la producción de uno nuevo. En su lugar, la
[publicación desde una sola fuente][la entrada]
([SSP]{.acronimo}) consiste en un proceso ramificado de
producción donde el archivo empleado para la redacción o
edición se procesa para distintas salidas.

Las salidas del archivo fuente pueden ser formatos
intermedios para la producción de otras salidas o pueden ser
un formato para el usuario final. Por ejemplo, una fuente en
[Markdown] puede procesarse para tener un [XML]{.acronimo}
que se aloja en un repositorio académico o para la obtención
de un [HTML]{.acronimo}, un [EPUB]{.acronimo} o un
[PDF]{.acronimo}. Existe más de una posibilidad para pasar
de una sola fuente a múltiples formatos, así que el camino a
recorrer depende de las necesidades de cada proyecto
editorial o de las capacidades de quienes lo realizan.

![Ejemplo de publicación desde una sola fuente, las
posibilidades de producción son relativas a la capacidad
técnica, la imaginación y la inventiva de quienes la llevan
a cabo.]

A esta fuente se le pueden agregar archivos de medios
---como imágenes, videos o audios--- o [fragmentos de texto
reutilizables] (*snippets*) ---ideales en los
[paratextos]---. Además, mediante identificadores es posible
vincular las entradas de una [base de datos bibliográfica],
lo que evita el uso [*hard-coded*] para las referencias y la
bibliografía, una fuente común de dificultades por su
habitual carencia de uniformidad. Para la estética de los
formatos finales es posible emplear plantillas y la
especificación del estilo de citas ---Citation Style
Language ([[CSL]]{.acronimo})---. Con el [CSL]{.acronimo} es
posible indicar o cambiar el modelo de citación a partir del
reemplazo o modificación de un archivo. Por ejemplo, sin la
manipulación del contenido el uso de [CSL]{.acronimo}
permite que un documento tenga un modelo de citación
[APA]{.acronimo} para alguna salida, mientras que para otra
puede tener un modelo personalizado de citación.

![En una sola fuente pueden vincularse medios, bibliografía,
plantillas, fragmentos de texto y modelos de citación.]

La metodología para la [publicación desde una sola
fuente][la entrada] evita el proceso de degradación de un
texto en cada bucle, como ocurre en la publicación
multiformato convencional, al mismo tiempo que permite la
producción automatizada o semiautomatizada de soportes. Sin
embargo, en comparación a un proceso de producción de
publicaciones afinado durante siglos, la corta edad de la
[SSP]{.acronimo} evidencia que aún hay un vasto trabajo por
realizar para que este modelo de producción satisfaga la
exigencia de la mayoría de las personas que se dedican a la
edición o a la autopublicación.

Al menos existen cuatro áreas en donde la [SSP]{.acronimo}
requiere de maduración:

1.  Experiencia de usuario. Las aplicaciones más robustas de
    la [SSP]{.acronimo} por lo general carecen de interfaces
    gráficas, exigen un dominio en el manejo de formatos o
    implican el conocimiento de varios lenguajes de
    programación o de marcado. Aún no hay un editor que con
    un clic produzca los formatos con las expectativas
    deseadas.
2.  Diseño. Por sus orígenes, el diseño para las
    publicaciones desde una sola fuente se especifica de
    manera programática. La consecuencia de esto es que las
    plantillas deben producirse a partir de la
    estructuración de datos, en lugar de elaborarse según
    las conveciones de diseño gráfico. Todavía no se cuenta
    con una [capa de abstracción] que permita el diseño de
    plantillas mediante una interfaz gráfica.
3.  Adopción de convenciones. Como la [SSP]{.acronimo}
    implica el manejo de múltiples formatos con uno o más
    programas de cómputo, no existe una metodología única
    para la publicación desde una sola fuente. La falta de
    convenciones en parte se debe a las técnicas disponibles
    para el desarrollo de *software* porque posibilitan la
    bifurcación o la producción de nuevos «estándares» sin
    que sean adoptados. No hay en la [SSP]{.acronimo} una
    idea fija sobre la metodología, los programas o los
    lenguajes aptos para la publicación desde una sola
    fuente.
4.  Manejo de excepciones. De manera inevitable cada formato
    tiene características que no están presentes en otro, o
    un usuario puede tener necesidades que no están
    soportadas en el lenguaje que utiliza para escribir el
    documento. La solución convencional de estas
    dificultades se inclina a la adición de
    preprocesamientos o posprocesamientos de la fuente que
    hacen más compleja la experiencia del usuario. Se
    requieren más propuestas que permitan el manejo de
    excepciones desde la misma fuente.

![«Cómo proliferan los estándares» muestra la manera
ordinaria en como las tecnologías disponibles pueden
menoscabar la adopción de convenciones.]

::: espacio-arriba
Pese a las tareas aún por resolver, en [Programando
[LIBRE]{.acronimo}ros][2] nos hemos inclinado al desarrollo
y a la experimentación de metodologías para la [publicación
desde una sola fuente][la entrada] debido a que se cuenta
con la certeza de cuáles son las áreas en las que la
[SSP]{.acronimo} palidece para su adopción por un amplio
grupo de editores o de autores. A diferencia de la
publicación cíclica convencional, en la [SSP]{.acronimo} es
posible mantener el control técnico y la calidad editorial
aunque conlleve algún [*trade-off*] en las áreas en donde no
hay maduración.
:::

Este tutorial pretende ofrecerte una solución a las
dificultades presentes en la publicación desde una sola
fuente. De manera específica, en este documento te ofrecemos
una propuesta de aplicación de esta metodología mediante
interfaces gráficas y sin la necesidad de contar con un
sistema operativo en específico. En [Programando
[LIBRE]{.acronimo}ros][2] sabemos que este tutorial no es la
respuesta definitiva a las dificultades actuales de la
[SSP]{.acronimo}, pero esperamos que satisfaga tus
necesidades editoriales o que sirva para que encuentres tus
propias soluciones.

[^1]: Para un maravilloso argumento en contra de los libros
    impresos y a favor de los libros hechos por copistas,
    ve: *Elogio de los amanuenses* de Johannes Trithemius,
    [UNAM]{.acronimo}, Ciudad de México, 2015; [disponible
    en línea].

  [la entrada]: https://es.wikipedia.org/wiki/Publicaci%C3%B3n_desde_una_sola_fuente
  [fe de erratas]: https://inciclopedia.org/wiki/Fe_de_erratas
  [epublibre]: https://epublibre.org
  [1]: static/img/titivillus.jpg
  [*desktop publishing*]: https://en.wikipedia.org/wiki/Desktop_publishing
  [*trade-off*]: https://es.wikipedia.org/wiki/Trade-off
  [2]: https://www.gitlab.com/prolibreros
  [Ejemplo de publicación cíclica, en cada nuevo bucle se aumenta la probabilidad de un menor control técnico y una inferior calidad editorial.]:
    static/img/publicacion-convencional.jpg
  [*bugs*]: https://es.wikipedia.org/wiki/Error_de_software
  [Zettlr]: https://www.zettlr.com
  [Pandoc]: https://pandoc.org
  [Scribus]: https://www.scribus.net
  [3]: https://es.wikipedia.org/wiki/GNU/Linux
  [Markdown]: https://es.wikipedia.org/wiki/Markdown
  [Ejemplo de publicación desde una sola fuente, las posibilidades de producción son relativas a la capacidad técnica, la imaginación y la inventiva de quienes la llevan a cabo.]:
    static/img/publicacion-ssp.jpg
  [fragmentos de texto reutilizables]: https://es.wikipedia.org/wiki/Snippet
  [paratextos]: https://es.wikipedia.org/wiki/Paratexto
  [base de datos bibliográfica]: https://es.wikipedia.org/wiki/Base_de_datos_bibliogr%C3%A1fica
  [*hard-coded*]: https://es.wikipedia.org/wiki/Hard_code
  [CSL]: https://es.wikipedia.org/wiki/Citation_Style_Language
  [En una sola fuente pueden vincularse medios, bibliografía, plantillas, fragmentos de texto y modelos de citación.]:
    static/img/fuente.jpg
  [capa de abstracción]: https://es.wikipedia.org/wiki/Capa_de_abstracci%C3%B3n
  [«Cómo proliferan los estándares» muestra la manera ordinaria en como las tecnologías disponibles pueden menoscabar la adopción de convenciones.]:
    static/img/standards.jpg
  [disponible en línea]: http://libgen.rs/book/index.php?md5=60B7C01C3FE0D9FCDA4629736BB3C382
