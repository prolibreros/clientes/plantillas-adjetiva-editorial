# Apéndice 2. Herramientas libres para la edición {#apéndice-2.-herramientas-libres-para-la-edición epub:type="appendix"}

Las herramientas que usamos en este tutorial ([Zettlr],
[Pandoc] y [Scribus]) son *software* libre. Si eres una
persona que de manera profesional se dedica a la edición de
libros, podría causarte interés o sospecha la posibilidad de
hacer libros con calidad editorial sin Adobe ni Microsoft
Office. Algunas de las preguntas que podrías tener son:

-   ¿Qué es el [*software* libre]?
-   ¿Cuáles son las posibilidades y los límites del
    [*software* libre]?
-   ¿Cuál es la conveniencia de usar herramientas que son
    [*software* libre]?

La libertad en el uso de *software* se entiende de distintas
maneras. La primera de ellas es comprender al [*software*
libre] como un programa de cómputo gratuito ([*freeware*]).

Si bien es cierto que el [*software* libre] permite su uso
gratuito, es relevante hacer la distinción respecto al
[*freeware*]. Ambos tipos de programas tienen la
característica de que el usuario puede usarlos sin la
mediación de un pago. Sin embargo, las licencias de
[*software* libre] exigen tener el [código fuente]
disponible, mientras que en el [*freeware*] es suficiente
con tener disponible el programa ejecutable ([código
máquina]).

La diferencia entre el [código fuente] y el [código máquina]
es que en la gran mayoría de los casos el [código fuente]
está escrito en algún lenguaje de programación. Estos
lenguajes están pensados para ser lo más inteligible por
humanos. Antes de los lenguajes de programación, el
*software* se programaba con ceros y unos, una
representación que era difícil de escribir, entender,
mantener y arreglar.

![Comparación entre el [código fuente] y el [código
máquina]. En este ejemplo el [código fuente] escrito en C++
indica que imprima un `hi` durante su ejecución en la
terminal. El proceso de compilación traduce este [código
fuente] a [código máquina] disponible en un archivo
ejecutable donde el mismo programa queda representado por
ceros y unos.][1]

El [*freeware*] solo tiene disponible el [código máquina]
para su ejecución, por lo que es difícil de modificar o
auditar. Por lo general esto es conveniente para el
desarrollador de [*freeware*], ya que le permite implementar
características indeseables para el usuario, como el uso de
publicidad, el monitero de su actividad o la instalación de
*software* malicioso ([*malware*]). Cuando un programa
gratuito es [*software* libre], estas características
indeseables son fáciles de detectar y corregir porque su
[código fuente] permite el escrutinio.

::: note
El [*freeware*] y el [*software* libre] son cosas distintas.
El [*freeware*] son programas ejecutables disponibles sin
costo pero sin la certeza de cómo funcionan. El [*software*
libre] son programas gratuitos que tienen su [código fuente]
disponible, lo que permite tener certeza sobre su
funcionamiento.
:::

La segunda manera de comprender al [*software* libre] es
como un programa de [código abierto].

En el ejercicio habitual de desarrollo y uso de *software*
son poco perceptibles las diferencias entre el [código
abierto] y el [*software* libre]. En ambos tipos de
programas se cuenta con el [código fuente] disponible y con
licencias de uso que permiten su estudio, modificación,
reutilización o distribución sin costo adicional y sin la
anuencia por escrito del titular de los derechos.

::: note
Como en la práctica los intereses del [*software* libre]
empatan con los del [código abierto], es común denominarlos
con el acrónimo [[FOSS]]{.acronimo} (*Free and Open-Source
Software*) o [[FLOSS][FOSS]]{.acronimo} (*Free/Libre
Open-Source Software*).
:::

Sin embargo, existe una diferencia legal en su licencia que
tiene repercusiones económicas, políticas y sociales. Las
licencias de [*software* libre] exigen que cualquier mejora
o modificación al programa sea publicado con una licencia de
[*software* libre]. Mientras tanto, las licencias de [código
abierto] permiten que las modificaciones al programa se
puedan reservar para un uso privado o para su
comercialización.

![En este diagrama de Venn se puede apreciar que el
[*software* libre], el [código abierto] y el [*freeware*]
tienen en común la disponibilidad del código máquina que
permite un uso gratuito del programa. Sin embargo, solo el
[*software* libre] y el [código abierto] comparten la
característica de tener disponible el código fuente del
programa, lo que permite su estudio y modificación.][2]

Un ejemplo permite aclarar esta diferencia. Android es quizá
el programa de [código abierto] más popular en la
actualidad. La disponibilidad de su [código fuente] permite
que los operadores de telecomunicaciones o los fabricantes
de teléfonos ofrezcan sistemas operativos Android con
modificaciones. Este es el motivo por el que los teléfonos
Android comparten muchas características entre sí al mismo
tiempo que tienen sus especificidades como las *apps*
preinstaladas o la estética del sistema.

La gran mayoría de estos operadores o fabricantes no tienen
disponible el [código fuente] de las modificaciones que le
han hecho al sistema base de Android porque su licencia de
[código abierto] permite reservarlo para su uso privado.
Aunque esto se justifica como modelo de negocio o
competitividad empresarial, la consecuencia es que vuelve
muy difícil la auditoría de estos sistemas. Esta falta de
certeza provoca la sospecha del daño potencial que se podría
ocasionar al usuario de Android, principalmente en lo que se
refiere al monitoreo de su actividad.

El carácter hereditario de la misma licencia es el mecanismo
en como el [*software* libre] ha intentado contrarrestar
estas consecuencias que atentan al ecosistema de tecnologías
libres. Aunque en un primer momento es paradójico ---el
[*software* libre] no te da la libertad de publicar tus
programas derivados con la licencia que te venga en gana---,
la lógica es la siguiente: para garantizar la libertad de
todos los usuarios de *software*, y así fomentar un
ecosistema, es necesario asegurar que las libertades no sean
removidas en su reutilización; por lo tanto, las licencias
de [*software* libre] exigen que sus programas derivados
sean publicados con la misma licencia.

::: note
El [código abierto] y el [*software* libre] son cosas
distintas. Los programas de [código abierto] permiten que el
[código fuente] de sus modificaciones no esté disponible.
Mientras tanto, los programas con licencias de [*software*
libre] exigen que el [código fuente] de sus modificaciones
estén disponibles para así conservar la libertad en el uso
de *software*.
:::

La tercera manera de comprender al [*software* libre] es en
contraposición al [*software* propietario].

El [*software* propietario] son los programas que no tienen
disponible su [código fuente] y que prohíben cualquier
modificación o redistribución de sus ejecutables. Para la
comercialización del [código máquina] al usuario se le
ofrece:

-   pago único como Affinity Publisher;
-   modelo de suscripción como Adobe InDesign, o
-   modelo [*freemium*] como Canva ---el usuario tiene
    acceso gratuito pero limitado al menos que pague por ser
    *premium*---.

Aunque la distinción es funcional, un matiz es necesario.
Una gran cantidad de [*software* propietario] depende del
[*software* libre] para su ejecución. Esto se debe a que los
programas de [*software* libre] en varias ocasiones son
usados como [bibliotecas] para los programas propietarios.
Con esto se busca la reducción en la carga de trabajo al
delegar ciertas funciones del programa a sus [bibliotecas].
Por ello, la contraposición entre [*software* propietario] y
[*software* libre] tiene sentido desde el punto de vista del
usuario, pero se matiza desde la perspectiva del
desarrollador que usa las [bibliotecas] para su programa
propietario.

::: note
El [*software* propietario] y el [*software* libre] son
cosas distintas. El [*software* propietario] no tiene
disponible su [código fuente] y prohíbe la modificación o
distribución de sus ejecutables. El [*software* libre] hace
público su [código fuente] y permite cualquier modificación
o distribución.
:::

Las tres maneras de comprender al [*software* libre] en
comparación al [*freeware*], al [código abierto] y al
[*software* propietario] nos permiten ahora tener más
claridad sobre la comprensión de las 4 libertades que el
[*software* libre] busca garantizar:

1.  Libertad de uso.
2.  Libertad de estudio.
3.  Libertad de distribución.
4.  Libertad de mejora.

Estas 4 libertades es lo que garantiza que un programa sea
[*software* libre] ya que para su ejercicio pleno se
requiere del acceso a su [código fuente] y a licencias que
permitan la modificación o distribución del programa, unas
normas distintas al [*freeware*] y al [*software*
propietario]. Para garantizar la continuidad de estas
libertades en los programas derivados el *software* libre
requiere la herencia del mismo tipo de licencia, la
característica que lo distingue del [código abierto].

De manera personal prefiero simplificar las 4 libertades a
la primera. Por un lado, el estudio, distribución y mejora
de un programa son ejercicios que se dan en *el uso* del
*software*. Por el otro, esto permite hacer patente que la
noción de «libertad» en el *software* es muy concreta: la
libertad es en relación con *el uso* del *software*. Esto
permite aclarar por qué, aunque la [filosofía del *software*
libre] aspira a un entendimiento de la libertad como
«sociedades libres», en la práctica y en lo legal la
libertad queda delimitada a *los usos* del *software*. Quizá
para evitar mayores ambigüedades en la intelección de la
libertad en el contexto del *software* sea más puntual
hablar de «*software* libre de uso» o «*software* de uso
libre» que de «[*software* libre]».

::: note
Un programa se considera [*software* libre] si su [código
fuente] está disponible y si su licencia indica que los
programas derivados también han de ser [*software* libre].
Solo así se garantizan las 4 libertades del *software*: las
libertades de uso, estudio, distribución y mejora.
:::

Para que un programa sea considerado [*software* libre] ---o
para que un producto cultural sea libre--- se requiere la
adición de una licencia que garantice su uso según las 4
libertades. Esta licencia no se opone al derecho de autor,
sino que lo flexibiliza. En un ejercicio ordinario del
[derecho de autor] la persona autora tiene todos los
derechos reservados sobre su obra. Cuando esta publica con
una licencia libre **no** pierde los derechos sobre su obra,
sino que se reserva algunos derechos ---por lo general son
el derecho de atribución y otros [derechos morales]---. Los
demás derechos, como el de uso, copia, modificación o
distribución son permitidos para cualquier persona.

Este traslado de «todos los derechos reservados» a «algunos
derechos reservados» permite que el usuario pueda usar
libremente una obra sin que el patrimonio del autor esté en
peligro. La persona autora aún tiene capacidad de decidir el
destino de su obra, así como puede tener mecanismos de
retribución. Pensar que el licenciamiento libre hace que el
autor regale su trabajo es un mito equivocado y recurrente
entre las personas o las entidades que ven en los ideales de
la cultura libre una amenaza a su sustento de vida o a su
modelo de negocios.

En la actualidad existen una gran cantidad de licencias
libres. Algunas están diseñadas para el *software*, otras
para base de datos u obras artísticas. [Zettlr] está
licenciado con [[GPL]]{.acronimo} versión 3, mientras que
[Pandoc] y [Scribus] se encuentran licenciados con
[[GPL]]{.acronimo} versión 2.

::: {.note .centrado}
Visita [este listado] para conocer muchas de las licencias
libres disponibles.
:::

Para la producción de libros con *software* estas libertades
fomentan un ecosistema con tecnologías disponibles sin
necesidad de un pago y que pueden ser amoldadas según los
intereses u objetivos de un proyecto editorial. Esto quiere
decir que en la edición con [*software* libre] se tiene la
oportunidad de elegir cómo ejecutar un proyecto editorial,
en lugar de delimitarlo a las posibilidades que nos ofrece
algún programa o compañía.

Una ventaja adicional en el uso de herramientas libres es
que estas tienden a consumir menos recursos. Por lo general
un programa libre tiene un tamaño de archivo menor y
necesita menos memoria [RAM]{.acronimo} o [CPU]{.acronimo}
para trabajar. Gracias a esto no hay necesidad de adquirir
equipo de cómputo o de contar con una máquina potente para
hacer una publicación con calidad editorial.

![El [*software* libre] busca fomentar un ecosistema en
donde los usuarios usan y comparten libremente los frutos de
su trabajo.][3]

Uno de los prejuicios sobre el [*software* libre] es la
suposición de que sus programas son de menor calidad en
relación con los programas propietarios. No obstante, como
se mencionó con anterioridad, la gran mayoría del
[*software* propietario] usa el [*software* libre] como
[biblioteca][bibliotecas] debido a su fiabilidad. La
cuestión tal vez tiene su origen a dos carencias recurrentes
en los programas libres destinados al usuario final:

1.  Una experiencia de usuario poco grata.
2.  Un entorno gráfico poco intuitivo.

No obstante, de manera paulatina el [*software* libre] está
ganando su lugar en el usuario final. Para quienes desean
hacer publicaciones con herramientas libres todavía hay
mucho camino por recorrer, aunque las bases ya están
presentes: flujos de trabajo eficaces, comunidades que dan
soporte incondicional y herramientas potentes y de gran
calidad.

  [Zettlr]: https://www.zettlr.com
  [Pandoc]: https://pandoc.org
  [Scribus]: https://www.scribus.net
  [*software* libre]: https://es.wikipedia.org/wiki/Software_libre
  [*freeware*]: https://es.wikipedia.org/wiki/Software_gratis
  [código fuente]: https://es.wikipedia.org/wiki/C%C3%B3digo_fuente
  [código máquina]: https://es.wikipedia.org/wiki/Lenguaje_de_m%C3%A1quina
  [1]: static/img/codigo.jpg
  [*malware*]: https://es.wikipedia.org/wiki/Malware
  [código abierto]: https://es.wikipedia.org/wiki/C%C3%B3digo_abierto
  [FOSS]: https://es.wikipedia.org/wiki/Software_libre_y_de_c%C3%B3digo_abierto
  [2]: static/img/venn.jpg
  [*software* propietario]: https://es.wikipedia.org/wiki/Software_propietario
  [*freemium*]: https://es.wikipedia.org/wiki/Freemium
  [bibliotecas]: https://es.wikipedia.org/wiki/Biblioteca_(inform%C3%A1tica)
  [filosofía del *software* libre]: https://www.gnu.org/philosophy/philosophy.es.html
  [derecho de autor]: https://es.wikipedia.org/wiki/Derecho_de_autor
  [derechos morales]: https://es.wikipedia.org/wiki/Derechos_morales
  [GPL]: https://es.wikipedia.org/wiki/GNU_General_Public_License
  [este listado]: https://www.gnu.org/licenses/license-list.html
  [3]: static/img/foss.jpg
