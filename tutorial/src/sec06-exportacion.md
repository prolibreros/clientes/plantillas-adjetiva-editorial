# Exportación del [HTML]{.acronimo} y del [EPUB]{.acronimo}

A través de la interfaz de [Zettlr] las primeras salidas son
un [EPUB]{.acronimo} y un [HTML]{.acronimo} hechos con
[Pandoc], un programa libre para la conversión de
documentos.

::: {.note .centrado}
No olvides visitar el [manual de Pandoc]
:::

El [EPUB]{.acronimo} puede subirse a las tiendas de Amazon,
Apple y Amazon, así como puede convertirse en formatos para
Kindle ---a través de [Kindle Previewer]--- para poder ser
ofrecido para estos dispositivos y fuera de Amazon.

El [HTML]{.acronimo} es para cotejo rápido y, si se desea,
para tener el libro en un micrositio *web*, como este
tutorial.

  [Zettlr]: https://www.zettlr.com
  [Pandoc]: https://pandoc.org
  [manual de Pandoc]: https://pandoc.org/MANUAL.html
  [Kindle Previewer]: https://kdp.amazon.com/es_ES/help/topic/G202131170
