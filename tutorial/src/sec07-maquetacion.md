# Maquetación del [PDF]{.acronimo}

La última salida de este tutorial es un [PDF]{.acronimo}
hecho con [Scribus], un programa libre de maquetación.

::: {.note .centrado}
No olvides visitar la [wiki de Scribus]
:::

  [Scribus]: https://www.scribus.net
  [wiki de Scribus]: https://wiki.scribus.net/canvas/Pagina_principal
