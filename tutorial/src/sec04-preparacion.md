# Preparación del proyecto

Para la producción de los formatos [HTML]{.acronimo},
[EPUB]{.acronimo} y [PDF]{.acronimo} de este tutorial
necesitaremos descargar el *dummy*, así como configurar
[Zettlr] y [Scribus]. Este es el proceso más delicado que
haremos: de su correcta ejecución depende que nuestros
formatos tengan las características deseadas. Por fortuna es
un procedimiento que solo requiere clics.

## Descarga del *dummy*

El *dummy* es un proyecto base que nos permite trabajar este
tutorial o cualquier otra publicación que quieras hacer con
este flujo de trabajo. Para su descarga haz lo siguiente:

1.  [Descarga este tutorial] en [ZIP]{.acronimo}.
2.  Descomprime el archivo `tutorial-ssp-no-masters.zip`.
3.  Entra a la carpeta `tutorial-ssp-no-masters`.
4.  Copia la carpeta `dummy`.
5.  Pega la carpeta `dummy` en donde desees alojar tu
    proyecto.
6.  Renombra la carpeta `dummy`; por convención le
    llamaremos [`librito`]{#librito} en este tutorial.

::: warn
Si por algún motivo no puedes descargar el tutorial en
[ZIP]{.acronimo}, entonces:

1.  Ve a su [repositorio].
2.  Da clic en el botón de descarga: ![][1]
3.  Selecciona `zip`.
:::

::: note
Si cuentas con [Git], prefiere este método de descarga con:

    git clone --depth 1 https://gitlab.com/prolibreros/docs/tutorial-ssp.git
:::

El *dummy* cuenta con esta estructura:

``` sh
dummy
├── metadata.yaml
├── sec01.md
└── static
    ├── css
    ├── defaults
    ├── filters
    ├── fonts
    ├── img
    │   └── portada.jpg
    ├── scripts
    └── templates
```

Los archivos que trabajaremos son `metadata.yaml` y los
Markdown, como la sección de ejemplo llamada `sec01.md`. En
el directorio `static` se encuentran los archivos
adicionales de nuestro proyecto. En la carpeta `static/img`
se coloca cualquier imagen que incluyas en tu publicación,
como la portada.

El resto de las carpetas dentro de `static` solo son
relevantes para el funcionamiento de [Zettlr], [Pandoc] o
[Scribus], así que no es necesario que las conozcas a
detalle. No obstante, si por algún motivo deseas
experimentar con la producción del [HTML]{.acronimo},
[EPUB]{.acronimo} o [PDF]{.acronimo}, siente la libertad de
jugar con los archivos dentro de `static`. Cualquier cambio
indeseado es revertible si respaldas tus imágenes y después
reemplazas la carpeta `static` de tu proyecto con la que se
encuentra dentro del *dummy*.

## Configuración de Zettlr

La configuración de [Zettlr] consiste en dos tareas:

1.  Modificación de las opciones de exportación. Esto nos
    permite producir el [HTML]{.acronimo}, el
    [EPUB]{.acronimo} y el proyecto de [Scribus] a partir de
    nuestro proyecto escrito con Markdown.
2.  Modificación de las opciones del proyecto. Esto nos
    sirve para especificar particularidades del proyecto
    como el título de la publicación y las salidas de
    exportación.

### Modificación de las opciones de exportación

Para las expotaciones, [Zettlr] utiliza archivos de
configuración de [Pandoc] que se conocen como `defaults`.
Los `defaults` son los valores por defecto para la
exportación de [Pandoc]. Para este tutorial los
modificaremos para obtener las tres salidas deseadas (el
[HTML]{.acronimo}, el [EPUB]{.acronimo} y el proyecto de
[Scribus]).

::: {.note .centrado}
En el [manual] de [Pandoc] puedes consultar las opciones
para los [`defaults`].
:::

Para comenzar, en [Zettlr][]:

1.  Ve a `Archivo > Preferencias > Gestor de recursos`.
2.  Selecciona la pestaña `Exportar` de la ventana flotante.

![Vista del gestor de recursos de [Zettlr] con la opción
`HTML` seleccionada de la pestaña `Exportar`.][2]

En la ventana abierta modificaremos las opciones de
exportación para `HTML`, `Plain Text` y `RTF`. Por defecto
[Zettlr] no cuenta con opciones de exportación para
[EPUB]{.acronimo} o proyectos de [Scribus]. Por este motivo
haremos dos pequeños *hacks*:

1.  La exportación `RTF` se usa para la exportación del
    [EPUB]{.acronimo}.
2.  La exportación `Plain Text` se usa para la exportación
    del proyecto de [Scribus].

::: {.warn .centrado}
Si por algún motivo quieres utilizar las exportaciones para
`Plain Text` o `RTF`, utiliza otras opciones de exportación
para el [EPUB]{.acronimo} y el proyecto de Scribus.
:::

Las nuevas opciones de exportación están en la carpeta
`static/defaults` de nuestro [librito] y son las siguientes:

-   `default.html` para la exportación del
    [HTML]{.acronimo}.
-   `default.epub` para la exportación del
    [EPUB]{.acronimo}.
-   `default.sla` para la exportación del proyecto de
    [Scribus].

Para añadirlas a [Zettlr] hacemos lo siguiente:

1.  Copia el contenido de `default.html` en `HTML` y da clic
    en `Guardar`.
2.  Copia el contenido de `default.epub` en `RTF` y da clic
    en `Guardar`.
3.  Copia el contenido de `default.sla` en `Plain Text` y da
    clic en `Guardar`.

![Vista del gestor de recursos de [Zettlr] con la opción
`HTML` **modificada** por el cotenido de `default.html`.][3]

Ahora puedes cerrar la ventana flotante del gestor de
recursos. Con esto terminamos las modificaciones de las
opciones de exportación de [Zettlr] y pasamos a configurar
el proyecto del [librito].

### Modificación de las opciones del proyecto

Para hacer las modificaciones del proyecto en [Zettlr]
abriremos el [librito] como un área de trabajo. Para ello en
[Zettlr][]:

1.  Ve a `Archivo > Abrir área de trabajo…`
2.  Selecciona la carpeta [`librito`][librito].

![Vista del panel izquierdo de [Zettlr] con el [librito]
como área de trabajo.][4]

A continuación abrimos las propiedades del área de trabajo
de [librito] con:

1.  Da clic derecho en [librito].
2.  Da clic en la opción `Propiedades`.

![A la izquierda, vista al hacer clic derecho en [librito].
A la derecha, vista al hacer clic en `Propiedades`.][5]

Con las propiedades abiertas, configuramos el área de
trabajo del [librito] con:

1.  Da clic en `Configuración del proyecto…`.
2.  Nombra tu publicación en la opción `Project Title` de la
    ventana flotante.
3.  Selecciona los formatos `HTML`, `Plain Text` y
    `RichText Format`.
4.  Cierra la ventana flotante.

::: {.warn .centrado}
Si te interesa generar más formatos, selecciónalos en este
momento.
:::

![Vista de la configuración del área de trabajo del
[librito].][6]

Con esto terminamos las configuraciones de [Zettlr] y
pasamos a configurar [Scribus].

## Configuración de Scribus

La configuración de [Scribus] consiste en una tarea:

1.  Adición de la plantilla. Esto nos permite maquetar el
    [PDF]{.acronimo} con estilos similares a los que se usan
    en el [HTML]{.acronimo} y el [EPUB]{.acronimo}.

### Adición de la plantilla

Para la maquetación, [Scribus] permite el uso de plantillas
que simplifican el trabajo de diseño. Por defecto [Scribus]
cuenta con varias plantillas disponibles. Sin embargo, para
este caso vamos a añadir una nueva plantilla que corresponde
con el estilo de los otros formatos de salida.

Para comenzar, en [Scribus][]:

1.  Ve a `Archivo > Preferencias`.
2.  Selecciona la pestaña `Rutas` de la ventana flotante.

![Ventana de las preferencias de [Scribus] con la pestaña
`Rutas` seleccionada.][7]

En la ventana abierta veremos la ruta que [Scribus] utiliza
para guardar las plantillas. Si no tiene ninguna
seleccionada, escoge una.

::: {.note .centrado}
En [[GNU]{.acronimo}/Linux][8] la ruta por defecto para las
plantillas de [Scribus] es:

    ~/.local/share/scribus/templates
:::

Ahora en tu explorador de archivos haz:

1.  Copia la carpeta `static/templates/pdf` de tu [librito].
2.  Pega esta carpeta en **tu ruta** a las plantillas de
    [Scribus].

De manera opcional puedes renombrar la carpeta `pdf`. Esto
es conveniente para que distingas la plantilla. Como esta es
para los estilos de [Adjetiva Editorial], la carpeta `pdf`
puedes renombrarla como `adjetiva`.

Para corroborar que la plantilla está añadida correctamente,
en [Scribus][]:

1.  Ve a `Archivo > Nuevo desde plantilla`.

Si fue añadida, deberías ver la plantilla de [Adjetiva
Editorial] como una de las opciones.

![Ventana de las plantillas de [Scribus].][9]

Con esto terminamos la preparación de nuestro proyecto y
pasamos a editar nuestro [librito].

  [Zettlr]: https://www.zettlr.com
  [Scribus]: https://www.scribus.net
  [Descarga este tutorial]: https://gitlab.com/prolibreros/docs/tutorial-ssp/-/archive/no-masters/tutorial-ssp-no-masters.zip
  [repositorio]: https://gitlab.com/prolibreros/docs/tutorial-ssp
  [1]: static/img/btn-descarga.jpg
  [Git]: https://es.wikipedia.org/wiki/Git
  [Pandoc]: https://pandoc.org
  [manual]: https://pandoc.org/MANUAL.html
  [`defaults`]: https://pandoc.org/MANUAL.html#defaults-files
  [2]: static/img/zettlr-gestor.jpg
  [librito]: #librito
  [3]: static/img/zettlr-gestor-mod.jpg
  [4]: static/img/zettlr-area.jpg
  [5]: static/img/zettlr-propiedades.jpg
  [6]: static/img/zettlr-configuracion.jpg
  [7]: static/img/scribus-rutas.jpg
  [8]: https://es.wikipedia.org/wiki/GNU/Linux
  [Adjetiva Editorial]: https://adjetiva.mx
  [9]: static/img/scribus-plantillas.jpg
