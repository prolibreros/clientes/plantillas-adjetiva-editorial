# Introducción {#introducción epub:type="foreword"}

Esta publicación es un tutorial para producir un libro en
formato [HTML]{.acronimo}, [EPUB]{.acronimo} y
[PDF]{.acronimo} a partir de un archivo [Markdown] según la
metodología de [publicación desde una sola fuente] y con
herramientas que son [*software* libre] o de [código
abierto]. El flujo de trabajo es el siguiente:

![Ejemplo de flujo para la [publicación desde una sola
fuente].][1]

Este tutorial fue redactado para la producción de libros
multiformato de [Adjetiva Editorial]. Sin embargo, también
es relevante para las personas interesadas en esta
metodología de producción o para quienes desean aprender a
hacer libros con [*software* libre] pero que no cuentan con
un sistema operativo [[GNU]{.acronimo}/Linux][2] o no
quieren recurrir al uso de una [terminal]. Si cuentas con
experiencia, este tutorial también puede hacerse con tu
editor favorito y tu terminal de preferencia sin necesidad
de modificar el árbol de directorios o los archivos de
configuración.

Este tutorial es un muestra de las capacidades de este tipo
de publicación, por lo que está para consulta y evaluación
en tres formatos:

-   [En línea]
-   [En [EPUB]{.acronimo}][3]
-   [En [PDF]{.acronimo}][4]

Para poder realizar este tutorial necesitas:

1.  Una computadora sin importar su sistema operativo.
2.  Conexión a internet para la descarga del proyecto base y
    del *software* necesario.

::: note
A lo largo del texto verás los siguientes recuadros:

::: {.note .centrado}
Recuadro para información relevante
:::

::: {.warn .centrado}
Recuadro de advertencia o para posibles dificultades
:::

::: {.error .centrado}
Recuadro para posibles errores
:::
:::

::: centrado
**¡Empecemos!**
:::

  [Markdown]: https://es.wikipedia.org/wiki/Markdown
  [publicación desde una sola fuente]: https://es.wikipedia.org/wiki/Publicaci%C3%B3n_desde_una_sola_fuente
  [*software* libre]: https://es.wikipedia.org/wiki/Software_libre
  [código abierto]: https://es.wikipedia.org/wiki/C%C3%B3digo_abierto
  [1]: static/img/flujo.jpg
  [Adjetiva Editorial]: https://adjetiva.mx
  [2]: https://es.wikipedia.org/wiki/GNU/Linux
  [terminal]: https://es.wikipedia.org/wiki/Interfaz_de_l%C3%ADnea_de_comandos
  [En línea]: https://prolibreros.gitlab.io/docs/tutorial-ssp
  [3]: https://prolibreros.gitlab.io/docs/tutorial-ssp/tutorial-ssp.epub
  [4]: https://prolibreros.gitlab.io/docs/tutorial-ssp/tutorial-ssp.pdf
