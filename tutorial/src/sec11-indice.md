# Índice analítico {#índice-analítico epub:type="index"}

::: frances
[Adjetiva Editorial]

[Arch Linux]

[base de datos bibliográfica]

[biblioteca o librería informática]

[*bugs*]

[capa de abstracción]

[código abierto]

[código fuente]

[código máquina]

[[CSL]]{.acronimo}

[Debian]

[derecho de autor (*copyright*)];\
[derechos morales]

[*desktop publishing*]

[*Elogio de los amanuenses*]

[epublibre]

[Fedora]

[fe de erratas]

[fragmentos de texto reutilizables]

[*freemium*]

[*freeware*]

[Git]

[Gnome]

[[GNU]{.acronimo}/Linux][1]

[*hard-coded*]

[Homebrew]

[Kindle Previewer]

[TeX]

[lenguaje de marcado ligero]

[librito]

[Linux Mint]

[*malware*]

[Markdown]

[Pandoc];\
[manual];\
[referencia de `defaults`][]:\
[referencia de `epub:type`]

[paratexto]

[Programando [LIBRE]{.acronimo}ros][2]

[publicación desde una sola fuente]

[Scribus];\
[página de descarga];\
[repositorio];\
[wiki]

[*software* libre];\
[filosofía];\
[[FOSS/FLOSS]]{.acronimo};\
[[GPL]]{.acronimo};\
[listado de licencias libres]

[*software* propietario]

[*splash screen*]

[terminal]

[*trade-off*]

[Tutorial en línea];\
[[EPUB]{.acronimo}][3];\
[[PDF]{.acronimo}][4];\
[repositorio][5];\
[[ZIP]{.acronimo}][6]

[Ubuntu]

[Zettlr];\
[página de descarga][7];\
[documentación]
:::

  [Adjetiva Editorial]: https://adjetiva.mx
  [Arch Linux]: https://www.archlinux.org
  [base de datos bibliográfica]: https://es.wikipedia.org/wiki/Base_de_datos_bibliogr%C3%A1fica
  [biblioteca o librería informática]: https://es.wikipedia.org/wiki/Biblioteca_(inform%C3%A1tica)
  [*bugs*]: https://es.wikipedia.org/wiki/Error_de_software
  [capa de abstracción]: https://es.wikipedia.org/wiki/Capa_de_abstracci%C3%B3n
  [código abierto]: https://es.wikipedia.org/wiki/C%C3%B3digo_abierto
  [código fuente]: https://es.wikipedia.org/wiki/C%C3%B3digo_fuente
  [código máquina]: https://es.wikipedia.org/wiki/Lenguaje_de_m%C3%A1quina
  [CSL]: https://es.wikipedia.org/wiki/Citation_Style_Language
  [Debian]: https://www.debian.org
  [derecho de autor (*copyright*)]: https://es.wikipedia.org/wiki/Derecho_de_autor
  [derechos morales]: https://es.wikipedia.org/wiki/Derechos_morales
  [*desktop publishing*]: https://en.wikipedia.org/wiki/Desktop_publishing
  [*Elogio de los amanuenses*]: http://libgen.rs/book/index.php?md5=60B7C01C3FE0D9FCDA4629736BB3C382
  [epublibre]: https://epublibre.org
  [Fedora]: https://getfedora.org
  [fe de erratas]: https://inciclopedia.org/wiki/Fe_de_erratas
  [fragmentos de texto reutilizables]: https://es.wikipedia.org/wiki/Snippet
  [*freemium*]: https://es.wikipedia.org/wiki/Freemium
  [*freeware*]: https://es.wikipedia.org/wiki/Software_gratis
  [Git]: https://es.wikipedia.org/wiki/Git
  [Gnome]: https://www.gnome.org
  [1]: https://es.wikipedia.org/wiki/GNU/Linux
  [*hard-coded*]: https://es.wikipedia.org/wiki/Hard_code
  [Homebrew]: https://brew.sh
  [Kindle Previewer]: https://kdp.amazon.com/es_ES/help/topic/G202131170
  [TeX]: https://es.wikipedia.org/wiki/TeX
  [lenguaje de marcado ligero]: https://es.wikipedia.org/wiki/Lenguaje_de_marcas_ligero
  [librito]: #librito
  [Linux Mint]: https://linuxmint.com
  [*malware*]: https://es.wikipedia.org/wiki/Malware
  [Markdown]: https://es.wikipedia.org/wiki/Markdown
  [Pandoc]: https://pandoc.org
  [manual]: https://pandoc.org/MANUAL.html
  [referencia de `defaults`]: https://pandoc.org/MANUAL.html#defaults-files
  [referencia de `epub:type`]: https://pandoc.org/MANUAL.html?pandocs-markdown#the-epubtype-attribute
  [paratexto]: https://es.wikipedia.org/wiki/Paratexto
  [2]: https://www.gitlab.com/prolibreros
  [publicación desde una sola fuente]: https://es.wikipedia.org/wiki/Publicaci%C3%B3n_desde_una_sola_fuente
  [Scribus]: https://www.scribus.net
  [página de descarga]: https://www.scribus.net/downloads/stable-branch
  [repositorio]: https://sourceforge.net/projects/scribus/files/scribus/1.4.8
  [wiki]: https://wiki.scribus.net/canvas/Pagina_principal
  [*software* libre]: https://es.wikipedia.org/wiki/Software_libre
  [filosofía]: https://www.gnu.org/philosophy/philosophy.es.html
  [FOSS/FLOSS]: https://es.wikipedia.org/wiki/Software_libre_y_de_c%C3%B3digo_abierto
  [GPL]: https://es.wikipedia.org/wiki/GNU_General_Public_License
  [listado de licencias libres]: https://www.gnu.org/licenses/license-list.html
  [*software* propietario]: https://es.wikipedia.org/wiki/Software_propietario
  [*splash screen*]: https://en.wikipedia.org/wiki/Splash_screen
  [terminal]: https://es.wikipedia.org/wiki/Interfaz_de_l%C3%ADnea_de_comandos
  [*trade-off*]: https://es.wikipedia.org/wiki/Trade-off
  [Tutorial en línea]: https://prolibreros.gitlab.io/docs/tutorial-ssp
  [3]: https://prolibreros.gitlab.io/docs/tutorial-ssp/tutorial-ssp.epub
  [4]: https://prolibreros.gitlab.io/docs/tutorial-ssp/tutorial-ssp.pdf
  [5]: https://gitlab.com/prolibreros/docs/tutorial-ssp
  [6]: https://gitlab.com/prolibreros/docs/tutorial-ssp/-/archive/no-masters/tutorial-ssp-no-masters.zip
  [Ubuntu]: https://ubuntu.com
  [Zettlr]: https://www.zettlr.com
  [7]: https://www.zettlr.com/download
  [documentación]: https://docs.zettlr.com/es
