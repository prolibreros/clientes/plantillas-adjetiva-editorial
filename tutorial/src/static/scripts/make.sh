SCR="./static/scripts/make.sh"
DEF="./static/defaults/default"
MDS="./*.md"
OUT="./pub"
FOR=false
OFF=false
COL=60

print_help() {
  echo "
make.sh: utilidad de procesamiento.

Uso:
  $SCR [OPCIONES]

Opciones:
  -o | --output     Indica nombre de las salidas SIN extensión; por defecto '$OUT'.
  -f | --format     Embellece los archivos Markdown; por defecto '$FOR'.
  -c | --columns    Indica caracteres por línea del embellecimiento; por defecto '$COL'.
  -n | --no-output  Evita producción de las salidas; por defecto '$OFF'.

Ejemplos:
  $SCR
    => Produce $OUT.html, $OUT.epub y $OUT.sla
  $SCR --output salida
    => Produce salida.html, salida.epub y salida.sla
  $SCR --output salida --format
    => Produce salida.html, salida.epub y salida.sla, y embellece los Markdown
  $SCR --output salida --format --columns 100
    => Produce salida.html, salida.epub y salida.sla, y embellece los Markdown con líneas de 100 caracteres
  $SCR --no-output --format --columns 100
    => No produce salidas, solo embellece los Markdown con líneas de 100 caracteres
  $SCR -n -f -c 100
    => Versión corta del ejemplo anterior
  "
}

invalid_opt() {
  echo "Opción inválida, imprimiendo ayuda."
  print_help
}

format() {
  for md in $MDS; do
    echo "[INFO] Formating $md"
    pandoc $md --reference-links --columns=$COL -o $md
  done
}

convert () {
  pandoc --defaults $DEF.html -o $OUT.html *.md
  pandoc --defaults $DEF.epub -o $OUT.epub *.md
  pandoc --defaults $DEF.sla  -o $OUT.sla  *.md
}

for arg in "$@"; do
  shift
  case "$arg" in
    '--help')      set -- "$@" '-h'   ;;
    '--output')    set -- "$@" '-o'   ;;
    '--format')    set -- "$@" '-f'   ;;
    '--columns')   set -- "$@" '-c'   ;;
    '--no-output') set -- "$@" '-n'   ;;
    *)             set -- "$@" "$arg" ;;
  esac
done

while getopts ":o:c:fnh" opt; do
  case $opt in
    'h') print_help; exit 0 ;;
    'o') OUT=$OPTARG ;;
    'f') FOR=true ;;
    'c') COL=$OPTARG ;;
    'n') OFF=true; FOR=true ;;
    '?') invalid_opt; exit 1 ;;
  esac
done

if [[ ! -d static ]]; then
  echo "No se encontró el directorio 'static'"
  echo "¿Te encuentras en la raíz del directorio?"
  exit 1
fi

if [ "$FOR" = true ]; then
  format
fi

if [ "$OFF" = false ]; then
  convert
fi
