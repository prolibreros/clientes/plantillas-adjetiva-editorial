# Edición de los metadatos y del contenido

::: {.warn .centrado}
En proceso de redacción
:::

## Edición de los metadatos

## Edición del contenido

Para la fuente de nuestro libro vamos a utilizar [Markdown],
un [lenguaje de marcado ligero] cuya filosofía es:

-   Fácil de leer
-   Fácil de escribir
-   Fácil de procesar

[Markdown] es un formato de archivo abierto, por lo que
puede trabajarse en cualquier editor de texto o de código.

Sin embargo, por conveniencia vamos a utlizar [Zettlr], un
programa libre para la edición de [Markdown].

::: {.note .centrado}
No olvides visitar la [documentación de Zettlr]
:::

```{=html}
<!--
Este es un comentario. 
Los comentarios no son visibiles en las conversiones.
Los comentarios son útiles para discutir sobre el libro.
-->
```
```{=html}
<!--
Para un EPUB podemos asignar nombres de secciones con el atributo 'epub:type'.
Para ver más nombres de secciones, visita:
-->
```

  [Markdown]: https://es.wikipedia.org/wiki/Markdown
  [lenguaje de marcado ligero]: https://es.wikipedia.org/wiki/Lenguaje_de_marcas_ligero
  [Zettlr]: https://www.zettlr.com
  [documentación de Zettlr]: https://docs.zettlr.com/es
