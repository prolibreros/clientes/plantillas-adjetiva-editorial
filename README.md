# Plantillas para Adjetiva Editorial

En este repositorio están las plantillas para los libros en PDF y _ebook_ de
Adjetiva Editorial.

## ¿Cómo usar las plantillas?

Ve el [tutorial] para aprender a usar las plantillas.

## Licencia

Estos archivos están bajo [Licencia Pública General de GNU][licencia]
(GPLv3).

[tutorial]: https://prolibreros.gitlab.io/docs/tutorial-ssp
[licencia]: https://es.wikipedia.org/wiki/GNU_General_Public_License
